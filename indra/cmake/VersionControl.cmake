# -*- cmake -*-
#
# Utility macros for getting info from the version control system.

# @return the rev number ie from the change set
macro(vcs_get_revision _output_variable)
	MESSAGE(STATUS "Checking for Git repository ${PROJECT_SOURCE_DIR}")
	
	# Check for the gitdirectory to see if this is a gitrepo
	IF(IS_DIRECTORY "${PROJECT_SOURCE_DIR}/../.git")
		# Find the gitexecutable for mac by bundle or by path and system path look everywhere!
		FIND_PROGRAM(_git git [PATH CMAKE_SYSTEM_PROGRAM_PATH CMAKE_SYSTEM_APPBUNDLE_PATH])
		
		# Test the return status make sure its found
		IF(NOT ${_git} MATCHES "-NOTFOUND")
			# Output GIT success
			MESSAGE(STATUS "Found git!")
			
			# Grab the revision number
			EXECUTE_PROCESS(COMMAND ${_git} summary WORKING_DIRECTORY 
			"${PROJECT_SOURCE_DIR}" OUTPUT_VARIABLE _release 
			RESULT_VARIABLE git_result
			OUTPUT_STRIP_TRAILING_WHITESPACE
			ERROR_STRIP_TRAILING_WHITESPACE)
			
			if (git_result EQUAL 0)
				# So some regex magic
				foreach(_v_l ${_release})
				if(_v_l MATCHES "^parent: *[^0-9]*\([0-9]+\):\([a-z0-9]+\)")
					# not using CPACK yet for installing. This is prep work for CPACK
					#set(CPACK_PACKAGE_VERSION_PATCH ${CMAKE_MATCH_1})
		
					# set Rev number.
					set(PHOENIX_WC_REVISION ${CMAKE_MATCH_1})
				endif()
				endforeach()
			ELSE ()
				# give feedback and set the rev to 0
				MESSAGE("Git had an error. Too old version of git?")
				SET(PHOENIX_WC_REVISION 0)
			ENDIF ()
		ELSE()
			# give feedback and set the rev to 0
			MESSAGE("Could not parse Git version. Newer version of git?")
			SET(PHOENIX_WC_REVISION 0)
		ENDIF()
	ELSE()
		# give feedback and set the rev to 0 as teh dirs not found
		MESSAGE(STATUS "Git not used..")
		SET(PHOENIX_WC_REVISION 0)
	ENDIF()
	 
	# CPACK framework
	#SET(PHOENIX_PACKAGE_VERSION ${V_MAJOR}.${V_MINOR}.${V_PATCH})
	
	# Dev build number framework.
	#SET(PHOENIX_DEVELOPMENT_VERSION 1)
	
	# Show the version number and complete / return to utility script.
	MESSAGE(STATUS "Current Git revision is ${PHOENIX_WC_REVISION}")
	set(${_output_variable} ${PHOENIX_WC_REVISION})
endmacro(vcs_get_revision)
